$('.dropify').dropify({
    messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove':  'Remove',
        'error':   'Ooops, something wrong happended.'
    }
});

var tag = $('#admin-content p:first:has("a")');
$('.page-titles .col-md-5.align-self-center').html(tag.html());
tag.remove();

$(document).on('change','#people-cat', function (e) {
    e.preventDefault();
   var id = this.value;
   $.ajax({
       url:'/admin/people/sub-cat',
       data: {id:id},
       type: 'get',
       success: function (res) {
           if(res)
               $('.sub-cat').html(res)
       },
       error: function (e) {
           console.log(e);
           alert('Системная ошибка')
       }
   })
});