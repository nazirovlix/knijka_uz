$(document).on('click', '.open-img', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/site/open-img',
        data: {id: id},
        type: 'get',
        success: function (res) {
            if (res)
                $('.modal-body').html(res);
            $('#noticeModal').modal();
        },
        error: function (e) {
            console.log(e);
            alert('Системная ошибка')
        }
    })

})