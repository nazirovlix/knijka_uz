<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'material/css/material-dashboard.css',
        'css/site.css',
    ];
    public $js = [
//        'material/js/core/jquery.min.js',
        'material/js/core/popper.min.js',
        'material/js/core/bootstrap-material-design.min.js',
        'material/js/plugins/perfect-scrollbar.jquery.min.js',
        'material/js/plugins/moment.min.js',
        'material/js/plugins/sweetalert2.js',
        'material/js/plugins/jquery.validate.min.js',
        'material/js/plugins/jquery.bootstrap-wizard.js',
        'material/js/plugins/bootstrap-selectpicker.js',
        'material/js/plugins/bootstrap-datetimepicker.min.js',
        'material/js/plugins/jquery.dataTables.min.js',
        'material/js/plugins/bootstrap-tagsinput.js',
        'material/js/plugins/jasny-bootstrap.min.js',
        'material/js/plugins/arrive.min.js',
        'material/js/plugins/bootstrap-notify.js',
        'material/js/material-dashboard.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
