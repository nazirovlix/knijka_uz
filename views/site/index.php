<div class="container-fluid">
    <div class="row">
        <?php foreach ($counts as $k => $item):?>
        <div class="col">
            <div class="card card-stats">
                <div class="card-header <?=  $k%2===0 ? 'card-header-warning' : 'card-header-info' ?> card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">person</i>
                    </div>
                    <h3 class="card-title"><?= $item['count'] ?>
                        <small>та</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger">list</i>
                        <a href="<?= \yii\helpers\Url::to(['/site/category','id' => $item['id']]) ?>"><?= $item['title'] ?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach;?>
        <div class="col">
            <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">notifications</i>
                    </div>
                    <h3 class="card-title"><?= $bcount ?>
                        <small>та</small>
                    </h3>
                </div>
                <div class="card-footer">
                    <div class="stats">
                        <i class="material-icons text-danger">list</i>
                        <a href="<?= \yii\helpers\Url::to(['/site/birthdays']) ?>">Тугилган кунлар</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header card-header-info">
                    <h4 class="card-title">Сунги кушилганлар</h4>
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                        <tr>
                            <th>#</th>
                            <th>Исми</th>
                            <th>Лавозими</th>
                            <th>Ички номер</th>
                            <th>4 талик номер</th>
                            <th>Хизмат номери</th>
                            <th>Мобил номери</th>
                            <th>Тугилган куни</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($people as $item):?>
                            <tr>
                                <td>
                                    <div class="round-img">
                                        <a href=""><img src="/uploads/<?=$item->image ?>" alt="" style="max-width: 50px"></a>
                                    </div>
                                </td>
                                <td><?= $item->fullname ?></td>
                                <td><?= $item->position ?></td>
                                <td><?= $item->int_num ?></td>
                                <td><?= $item->ip_num ?></td>
                                <td><?= $item->home_num ?></td>
                                <td><?= $item->phone ?></td>
                                <td><?= date('d.m.Y',$item->date) ?></td>
                            </tr>
                        <?php endforeach;?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>