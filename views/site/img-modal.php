<?php
?>

<div style="text-align: center">
    <div class="row center">
        <div class="col">
            <div class="picture card-avatar">
                <img src="/uploads/<?= $person->image ?>" alt="Thumbnail Image" class="rounded img-fluid" style="max-width: 300px">
            </div>
        </div>
    </div>
    <h4 style="margin-top: 10px"><?= $person->fullname ?></h4>
    <p style="margin-top: 10px"><?= date('d.m.Y',$person->date)?></p>
</div>


