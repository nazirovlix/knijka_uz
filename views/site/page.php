<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $subCat->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">assignment</i>
                    </div>
                    <h4 class="card-title"><?= $subCat->title ?></h4>
                </div>
                <div class="card-body">
                    <div class="toolbar">
                        <!--        Here you can write extra buttons/actions for the toolbar              -->
                    </div>
                    <div class="material-datatables">
                        <div id="datatables_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'fullname',
                                    'position',
                                    'int_num',
                                    'ip_num',
                                    'home_num',
                                    'phone',
                                    'date:date',
                                    [
                                        'attribute' => 'image',
                                        'value' => function ($model) {
                                            return '<button class="btn btn-info open-img" data-id="'. $model->id.'">Куриш</button>';
                                        },
                                        'format' => 'raw'
                                    ],
                                    //'created_at',
                                    //'updated_at',
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
                <!-- end content-->
            </div>
            <!--  end card  -->
        </div>
    </div>
</div>

<div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">close</i>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-danger btn-round" data-dismiss="modal">Беркитиш</button>
            </div>
        </div>
    </div>
</div>