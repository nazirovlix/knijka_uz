<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Categories;
use app\models\People;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
$categories = Categories::find()->all();
Yii::$app->session->open();
$activeMenu = !empty(Yii::$app->session->get('activeMenu')) ? Yii::$app->session->get('activeMenu') : 'home';

$bcount = People::find()
    ->where('month(FROM_UNIXTIME(date)) = month(current_date) and dayofmonth(FROM_UNIXTIME(date)) > dayofmonth(current_date) and dayofmonth(FROM_UNIXTIME(date)) < (dayofmonth(current_date) + 2)')
    ->count();

?>
?>

<div class="wrapper">
    <div class="sidebar" data-color="azure" data-background-color="white" data-image="/material/img/sidebar-1.jpg">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
            <a href="/" class="simple-text logo-normal">
                Книжка
            </a>
        </div>
        <div class="sidebar-wrapper">
            <ul class="nav">
                <li class="nav-item <?= $activeMenu === 'home' ? 'active' : '' ?>">
                    <a class="nav-link" href="/">
                        <i class="material-icons">dashboard</i>
                        <p>Асосий</p>
                    </a>
                </li>
                <?php foreach ($categories as $k => $category): ?>
                    <li class="nav-item <?= $activeMenu === $category->id ? 'active' : '' ?>">
                        <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples<?= $k ?>"
                           aria-expanded="false">
                            <i class="material-icons">content_paste</i>
                            <p> <?= $category->title ?>
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse <?= $activeMenu === $category->id ? 'show' : '' ?>" id="pagesExamples<?= $k ?>" style="">
                            <ul class="nav">
                                <?php if (!empty($category->subCategories)) {
                                    foreach ($category->subCategories as $item):?>
                                        <li class="nav-item <?= Yii::$app->request->get('id') == $item->id ? 'active' : '' ?>">
                                            <a class="nav-link"
                                               href="<?= \yii\helpers\Url::to(['/site/page', 'id' => $item->id]) ?>">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span class="sidebar-normal"> <?= $item->title ?> </span>
                                            </a>
                                        </li>
                                    <?php endforeach;
                                } ?>
                            </ul>
                        </div>
                    </li>
                <?php endforeach; ?>
                <li class="nav-item <?= $activeMenu === 'birthday' ? 'active' : '' ?>">
                    <a class="nav-link" href="<?= \yii\helpers\Url::to(['/site/birthdays']) ?>">
                        <i class="material-icons">notifications</i>
                        <p>Тугилган кунлар</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <a class="navbar-brand" href="/">Асосий</a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">

                    <ul class="navbar-nav">

                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?= \yii\helpers\Url::to(['/site/page', 'id' => $item->id]) ?>">
                                <i class="material-icons">notifications</i>
                                <span class="notification"><?=$bcount ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
            <?= $content ?>
        </div>
        <footer class="footer">
            <div class="container-fluid">
                <div class="copyright float-right">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>

                </div>
            </div>
        </footer>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
