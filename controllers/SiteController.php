<?php

namespace app\controllers;

use app\models\Birthday;
use app\models\Birthdays;
use app\models\Categories;
use app\models\forms\Login;
use app\models\People;
use app\models\PeopleCatSearch;
use app\models\PeopleSearch;
use app\models\SubCategory;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'home';
        Yii::$app->session['activeMenu'] = 'home';
        $categories = Categories::find()->all();
        $counts = [];
        foreach ($categories as $item) {
            $counts[] = [
                'count' => People::find()
                    ->joinWith('category')
                    ->where(['sub_category.category_id' => $item->id])
                    ->count(),
                'title' => $item->title,
                'id' => $item->id
            ];
        }
        $people = People::find()
            ->orderBy(['id' => SORT_DESC])
            ->limit(15)
            ->all();
        $bcount = People::find()
            ->where('month(FROM_UNIXTIME(date)) = month(current_date) and dayofmonth(FROM_UNIXTIME(date)) > dayofmonth(current_date) and dayofmonth(FROM_UNIXTIME(date)) < (dayofmonth(current_date) + 2)')
            ->count();

        return $this->render('index',[
            'counts' => $counts,
            'people' => $people,
            'bcount' => $bcount
        ]);
    }

    public function actionCategory($id)
    {
        $subCat = Categories::findOne($id);
        $searchModel = new PeopleCatSearch();
        Yii::$app->session['activeMenu'] = '99';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('page', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'subCat' => $subCat
        ]);
    }

    public function actionPage($id)
    {
        $subCat = SubCategory::findOne($id);
        Yii::$app->session['activeMenu'] = $subCat->category_id;
        $searchModel = new PeopleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);

        return $this->render('page', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'subCat' => $subCat
        ]);
    }
    public function actionBirthdays()
    {
        $subCat = new SubCategory();
        $subCat->title = Yii::t('app','Охирги 10 кун ичида Тугилган кунлар');
        Yii::$app->session['activeMenu'] = 'birthday';
        $searchModel = new Birthdays();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new Birthday();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);

        return $this->render('birthdays', [
            'searchModel' => $searchModel,
            'searchModel2' => $searchModel2,
            'dataProvider' => $dataProvider,
            'dataProvider2' => $dataProvider2,
            'subCat' => $subCat
        ]);
    }

    public function actionOpenImg($id)
    {
        $person = People::findOne($id);
        return $this->renderAjax('img-modal',['person' => $person]);
    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }


}
