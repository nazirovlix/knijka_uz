<!-- Start Page Content -->
<div class="row">
    <?php foreach ($counts as $k => $item):?>
    <div class="col">
        <div class="card p-30">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="fa fa-user f-s-40 <?= $k % 2 === 0 ? 'color-primary' : 'color-danger' ?>"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h2><?= $item['count'] ?></h2>
                    <p class="m-b-0"><?= $item['title'] ?></p>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-title">
                <h4>Сунги кушилганлар </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Исми</th>
                            <th>Лавозими</th>
                            <th>Ички номер</th>
                            <th>4 талик номер</th>
                            <th>Хизмат номери</th>
                            <th>Мобил номери</th>
                            <th>Тугилган куни</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($people as $item):?>
                        <tr>
                            <td>
                                <div class="round-img">
                                    <a href=""><img src="/uploads/<?=$item->image ?>" alt=""></a>
                                </div>
                            </td>
                            <td><?= $item->fullname ?></td>
                            <td><?= $item->position ?></td>
                            <td><?= $item->int_num ?></td>
                            <td><?= $item->ip_num ?></td>
                            <td><?= $item->home_num ?></td>
                            <td><?= $item->phone ?></td>
                            <td><?= date('d.m.Y',$item->date) ?></td>
                        </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- End PAge Content -->