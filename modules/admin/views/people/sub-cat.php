<div class="form-group field-people-category_id has-success">
    <label class="control-label" for="people-category_id">Sub Category</label>
    <select id="people-category_id" class="form-control" name="People[category_id]" aria-invalid="false">
        <?php foreach ($subcat as $item):?>
        <option value="<?= $item->id ?>"><?=$item->title ?></option>
        <?php endforeach;?>
    </select>

    <div class="help-block"></div>
</div>