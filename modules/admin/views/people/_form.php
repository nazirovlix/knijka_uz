<?php

use app\models\BaseModel;
use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\People */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="people-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col">
            <?= $form->field($model, 'cat')->dropDownList(
                \yii\helpers\ArrayHelper::map(\app\models\Categories::find()->all(), 'id', 'title')
                , ['prompt' => 'Категорияни танланг']) ?>

        </div>
        <div class="col sub-cat">
            <?php if ($model->isNewRecord): ?>
                <?= $form->field($model, 'category_id')->dropDownList(
                    []
                ) ?>
            <?php else: ?>
            <?= $form->field($model, 'category_id')->dropDownList(
                \yii\helpers\ArrayHelper::map($subcat, 'id', 'title')
            ) ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<div class="row">
    <div class="col">
        <?= $form->field($model, 'fullname')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col">
        <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <div class="col"><?= $form->field($model, 'int_num')->textInput() ?></div>
    <div class="col"><?= $form->field($model, 'ip_num')->textInput() ?></div>
</div>

<div class="row">
    <div class="col"><?= $form->field($model, 'home_num')->textInput() ?></div>
    <div class="col"><?= $form->field($model, 'phone')->textInput() ?></div>
</div>

<div class="row">
    <div class="col"><?= $form->field($model, 'base_file')->widget(trntv\filekit\widget\Upload::className(), ['url' => app\models\BaseModel::FILE_UPLOAD_URL, 'maxFileSize' => 1000000]) ?></div>
    <div class="col"><?= $form->field($model, 'date')->textInput(['type' => 'date']) ?></div>
</div>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

</div>
