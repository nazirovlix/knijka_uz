<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;
use yii\helpers\Url;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => [ 'id' => 'sidebarnav' ],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => [ 'class' => 'nav-devider' ]
                        ],
                        [
                            'label' => 'Home',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Асосий',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-tachometer"></i>',
                        ],
                        [
                            'label' => 'App',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Категориялар',
                            'url' => ['categories/index'],
                            'icon' => '<i class="fa  fa-file-o"></i>',
                        ],
                        [
                            'label' => 'Суб категориялар',
                            'url' => ['sub-category/index'],
                            'icon' => '<i class="fa  fa-file-o"></i>',
                        ],
                        [
                            'label' => 'Одамлар',
                            'url' => ['people/index'],
                            'icon' => '<i class="fa  fa-user"></i>',
                        ],

                    ]
                ]);
            } catch ( Exception $e ) {
            }
            
            ?>
        </nav>
    </div>
</div>
