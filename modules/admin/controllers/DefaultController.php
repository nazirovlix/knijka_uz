<?php

namespace app\modules\admin\controllers;

use app\models\Categories;
use app\models\People;
use mdm\admin\models\form\Login;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $categories = Categories::find()->all();
        $people = People::find()
            ->orderBy(['id' => SORT_DESC])
            ->limit(15)
            ->all();
        $counts = [];
        foreach ($categories as $item) {
            $counts[] = [
                'count' => People::find()->where(['category_id' => $item->id])->count(),
                'title' => $item->title
            ];
        }


        return $this->render('index',[
            'counts' => $counts,
            'people' => $people
        ]);
    }

    /**
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new Login();

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
}
