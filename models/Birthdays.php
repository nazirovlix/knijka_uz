<?php

namespace app\models;

use app\models\People;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PeopleSearch represents the model behind the search form of `app\models\People`.
 */
class Birthdays extends People
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'int_num', 'ip_num', 'home_num', 'phone', 'date', 'created_at', 'updated_at'], 'integer'],
            [['fullname', 'position', 'image'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = People::find()
            ->where('month(FROM_UNIXTIME(date)) = month(current_date)
and dayofmonth(FROM_UNIXTIME(date)) > dayofmonth(current_date) and dayofmonth(FROM_UNIXTIME(date)) < (dayofmonth(current_date) + 29)')
            ->orderBy(['date' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'int_num' => $this->int_num,
            'ip_num' => $this->ip_num,
            'home_num' => $this->home_num,
            'phone' => $this->phone,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'fullname', $this->fullname])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
