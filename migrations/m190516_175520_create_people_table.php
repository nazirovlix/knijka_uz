<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%people}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%sub_category}}`
 */
class m190516_175520_create_people_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%people}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'fullname' => $this->string(),
            'position' => $this->string(),
            'int_num' => $this->integer(),
            'ip_num' => $this->integer(),
            'home_num' => $this->integer(),
            'phone' => $this->integer(),
            'date' => $this->integer(),
            'image' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-people-category_id}}',
            '{{%people}}',
            'category_id'
        );

        // add foreign key for table `{{%sub_category}}`
        $this->addForeignKey(
            '{{%fk-people-category_id}}',
            '{{%people}}',
            'category_id',
            '{{%sub_category}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%sub_category}}`
        $this->dropForeignKey(
            '{{%fk-people-category_id}}',
            '{{%people}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-people-category_id}}',
            '{{%people}}'
        );

        $this->dropTable('{{%people}}');
    }
}
