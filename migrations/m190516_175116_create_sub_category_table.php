<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sub_category}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%categories}}`
 */
class m190516_175116_create_sub_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sub_category}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'title' => $this->string()->notNull(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-sub_category-category_id}}',
            '{{%sub_category}}',
            'category_id'
        );

        // add foreign key for table `{{%categories}}`
        $this->addForeignKey(
            '{{%fk-sub_category-category_id}}',
            '{{%sub_category}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%categories}}`
        $this->dropForeignKey(
            '{{%fk-sub_category-category_id}}',
            '{{%sub_category}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-sub_category-category_id}}',
            '{{%sub_category}}'
        );

        $this->dropTable('{{%sub_category}}');
    }
}
